#![feature(core_intrinsics)]
#![feature(zero_one)]
#![allow(dead_code)]
#![allow(unused_variables)]

extern crate rand;
extern crate time;
extern crate gtprust as gr;
extern crate image;

mod go;
mod gtp;

use std::error::Error;
use std::fs::File;
use std::io;
use std::path::Path;

use time::precise_time_ns;

fn main() {
  use std::io::{Read, Write};
  use std::str::FromStr;
  use go::*;

  let mut goban = Goban::new(19, 19);
  let mut stdin_buf = String::new();
  let mut move_sequence = Vec::new();
  loop {
    println!("{}", goban.repr(true, false));

    loop {
      print!(">>> ");
      let _ = io::stdout().flush();
      stdin_buf.clear();
      match io::stdin().read_line(&mut stdin_buf) {
        Err(err) => panic!("error while reading from stdin: {}", err),
        Ok(_) => {},
      }
      let cmd = stdin_buf.to_lowercase().trim().to_string();
      let mut words = cmd.split_whitespace();
      match words.next() {
        Some("b") => {
          while let Some(arg) = words.next() {
            let mov = Move::new(Color::Black, Coord::from_gtp(arg));
            println!("{}", mov);
            match goban.push_move(mov) {
              Err(err) => println!("{:?} can't play at {:?}: {:?}", mov.color, mov.pos, err),
              Ok(_) => {},
            }
          }
          break;
        }
        Some("w") => {
          while let Some(arg) = words.next() {
            let mov = Move::new(Color::White, Coord::from_gtp(arg));
            println!("{}", mov);
            match goban.push_move(mov) {
              Err(err) => println!("{:?} can't play at {:?}: {:?}", mov.color, mov.pos, err),
              Ok(_) => {},
            }
          }
          break;
        }
        Some("c") | None => {
          if move_sequence.len() > 0 {
            let mov = move_sequence.remove(0);
            println!("{}", mov);
            match goban.push_move(mov) {
              Err(err) => println!("{:?} can't play at {:?}: {:?}", mov.color, mov.pos, err),
              Ok(_) => {},
            }
            break;
          } else {
            println!("(cont.) No sequence.");
          }
        }
        Some("show") => {
          println!("{}", goban.repr(true, words.next().is_some()));
        }
        Some("showliberties") => {
          let cores =
            if let Some(coord) = words.next() {
              vec![goban.core_of(Coord::from_gtp(coord))]
            } else {
              goban.chain_cores.keys().cloned().collect::<Vec<_>>()
            };

          let mut scratch = [Coord(0, 0); 361];
          let mut total = 0;
          for core in cores {
            let for_this_core = goban.liberties_of(core, &mut scratch[total..]);
            println!("{:?}: {}", core, for_this_core);
            total += for_this_core;
          }
          println!("{}", draw_points(19, 19, true, &scratch[..total], '#'));
        }
        Some("enumcores") => {
          for (core, col) in &goban.chain_cores {
            println!("{:?} {:?}", col, core);
          }
        }
        Some("showcores") => {
          let cores = goban.chain_cores.keys().cloned().collect::<Vec<_>>();
          println!("{}", draw_points(19, 19, true, &cores, '#'));
        }
        Some("showcontour") => {
          let cores =
            if let Some(coord) = words.next() {
              vec![goban.core_of(Coord::from_gtp(coord))]
            } else {
              goban.chain_cores.keys().cloned().collect::<Vec<_>>()
            };

          let mut scratch = [Coord(0, 0); 361];
          let mut total = 0;
          for core in cores {
            let for_this_core = goban.contour_of(core, &mut scratch[total..]);
            println!("{:?}: {}", core, for_this_core);
            total += for_this_core;
          }
          for pt in &scratch[..total] {
            println!("{:?}", pt);
          }
          println!("{}", draw_points(19, 19, true, &scratch[..total], '#'));
        }
        Some("enumlibcache") => {
          let cores =
            if let Some(coord) = words.next() {
              vec![goban.core_of(Coord::from_gtp(coord))]
            } else {
              goban.chain_cores.keys().cloned().collect::<Vec<_>>()
            };

          let mut scratch = [Coord(0, 0); 361];
          for core in cores {
            let for_this_core = goban.chain_liberties[&core];
            println!("{:?}: {} (vs {} real)", core, for_this_core, goban.liberties_of(core, &mut scratch));
          }
        }
        Some("libpoints") => {
          let arg = words.next().unwrap();
          let pos = Coord::from_gtp(arg);
          let libs = Liberties::all_at(pos, goban.width, goban.height);
          print!("{} liberties at {}: ", libs.count(), pos);
          for &pt in libs.points(pos).iter() {
            if let Some(coord) = pt {
              print!("{:?} ", coord);
            }
          }
          println!("");
        }
        Some("suicides") => {
          match words.next() {
            Some("b") => {
              let points = goban.suicide_points[Color::Black as usize].iter().cloned().collect::<Vec<_>>();
              println!("{}", draw_points(19, 19, true, &points[..], '#'));
            }
            Some("w") => {
              let points = goban.suicide_points[Color::White as usize].iter().cloned().collect::<Vec<_>>();
              println!("{}", draw_points(19, 19, true, &points[..], '#'));
            }
            Some(_) | None => {
              println!("Please specify color.");
            }
          }
        }
        Some("ko") => {
          match goban.ko_point {
            None => println!("There is no ko at the moment."),
            Some(mov) => println!("{:?}", mov),
          }
        }
        Some("set") => {
          match words.next().unwrap() {
            "libcache" => {
              let pos = words.next().unwrap();
              let amt = u8::from_str(words.next().unwrap()).unwrap();
              goban.chain_liberties.insert(Coord::from_gtp(pos), amt);
            }
            name => {
              println!("I don't know what {} is.", name);
            }
          }
        }
        Some("rmdead") => {
          let mut scratch = [Coord(-1, -1); 361];
          match words.next().unwrap() {
            "w" => {
              goban.remove_dead_chains(Color::White, &mut scratch);
              break;
            }
            "b" => {
              goban.remove_dead_chains(Color::Black, &mut scratch);
              break;
            }
            c => {
              println!("{} is not a color.", c);
            }
          }
        }
        Some("parent") => {
          let arg = words.next().unwrap();
          println!("{:?}", goban.stone_parents.get(&Coord::from_gtp(arg)));
        }
        Some("reset") => {
          goban = Goban::new(19, 19);
          println!("Goban has been reset.");
          break;
        }
        Some("playout") => {
          let seq_path = Path::new(words.next().unwrap());
          let display = seq_path.display();
          let mut file = match File::open(&seq_path) {
            Err(why) => panic!("couldn't open {}: {}", display,
                                                       Error::description(&why)),
            Ok(file) => file,
          };
          let mut s = String::new();
          match file.read_to_string(&mut s) {
            Err(why) => panic!("couldn't read {}: {}", display,
                                                       Error::description(&why)),
            Ok(_) => {},
          };

          move_sequence = s.lines().map(|line| {
            let mut words = line.split_whitespace();
            let color = match words.next() {
              Some("B") => Color::Black,
              Some("W") => Color::White,
              Some(line) => panic!("bad token: {}", line),
              None => panic!("unexpected end of line"),
            };
            let mut coords = words.next().unwrap().bytes().map(|b| b - 'a' as u8);
            let pos = Coord(coords.next().unwrap() as i8, coords.next().unwrap() as i8).transposed();
            Move::new(color, pos)
          }).collect();
          println!("Sequence loaded successfully.");
        }
        Some("timeseq") => {
          if move_sequence.len() == 0 {
            println!("No sequence.");
            continue;
          }
          goban = Goban::new(19, 19);
          let old_sequence = move_sequence.clone();
          move_sequence.reverse();
          let mut mit = 0;
          let start = precise_time_ns();
          while let Some(m) = move_sequence.pop() {
            if let Err(_) = goban.push_move(m) {
              println!("Impl error at move {} ({})", mit, m);
              break;
            }
            mit += 1;
          }
          let end = precise_time_ns();
          println!("took {} miliseconds", (end - start) / 1_000_000);
          move_sequence = old_sequence;
        }
        Some(_) => {
          println!("I don't know how to interpret this.");
        }
      }
    }
  }
}
