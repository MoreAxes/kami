use go::*;
use heuristics;
use heuristics::McOwnerStat;

use std::f32;
use std::io;
use std::io::Write;

use rand::{Rng, sample};

// pub fn minmax<R: Rng>(goban: &mut Goban, depth: usize, color: Color, rng: &mut R) -> (f32, Coord) {
//   let mut b_infl_map = [0f32; 361];
//   let b_infl = heuristics::flood_influence(goban, Color::Black, &mut b_infl_map, None);
//   let mut w_infl_map = [0f32; 361];
//   let w_infl = heuristics::flood_influence(goban, Color::White, &mut w_infl_map, None);

//   match color {
//     Color::Black => {
//       let mut best_value = f32::NEG_INFINITY;
//       let mut best_move = Coord(-1, -1);
//       let legal_moves = goban.legal_moves(color);
//       for coord in heuristics::interesting_moves(goban, &legal_moves, color, 10, rng) {
//         let mut new_goban = goban.clone();
//         new_goban.push_move(coord.as_move(color));
//         if depth > 1 {
//           let (new_value, new_move) = minmax(&mut new_goban, depth - 1, color.other(), rng);
//           if new_value > best_value {
//             best_value = new_value;
//             best_move = coord;
//           }
//         } else {
//           let mut b_infl_map = [0f32; 361];
//           let b_infl = heuristics::flood_influence(&mut new_goban, Color::Black, &mut b_infl_map, None);
//           let mut w_infl_map = [0f32; 361];
//           let w_infl = heuristics::flood_influence(&mut new_goban, Color::White, &mut w_infl_map, None);
//           return (heuristics::aux_score(&mut new_goban, color.other(), &b_infl_map, &w_infl_map), coord);
//         }
//       }
//       (best_value, best_move)
//     }
//     Color::White => {
//       let mut best_value = f32::INFINITY;
//       let mut best_move = Coord(-1, -1);
//       let legal_moves = goban.legal_moves(color);
//       for coord in heuristics::interesting_moves(goban, &legal_moves, color, 10, rng) {
//         let mut new_goban = goban.clone();
//         new_goban.push_move(coord.as_move(color));
//         if depth > 1 {
//           let (new_value, new_move) = minmax(&mut new_goban, depth - 1, color.other(), rng);
//           if new_value < best_value {
//             best_value = new_value;
//             best_move = coord;
//           }
//         } else {
//           let mut b_infl_map = [0f32; 361];
//           let b_infl = heuristics::flood_influence(&mut new_goban, Color::Black, &mut b_infl_map, None);
//           let mut w_infl_map = [0f32; 361];
//           let w_infl = heuristics::flood_influence(&mut new_goban, Color::White, &mut w_infl_map, None);
//           return (heuristics::aux_score(&mut new_goban, color.other(), &b_infl_map, &w_infl_map), coord);
//         }
//       }
//       (best_value, best_move)
//     }
//     _ => (0f32, Coord(-1, -1)),
//   }
// }

// pub fn alpha_beta<R: Rng>(goban: &mut Goban, depth: usize, mut alpha: f32, mut beta: f32, color: Color, rng: &mut R) -> (f32, Coord) {
//   let mut b_infl_map = [0f32; 361];
//   let b_infl = heuristics::flood_influence(goban, Color::Black, &mut b_infl_map, None);
//   let mut w_infl_map = [0f32; 361];
//   let w_infl = heuristics::flood_influence(goban, Color::White, &mut w_infl_map, None);

//   match color {
//     Color::Black => {
//       let mut best_value = f32::NEG_INFINITY;
//       let mut best_move = Coord(-1, -1);
//       let legal_moves = goban.legal_moves(color);
//       let mut moves = heuristics::important_moves(goban, &legal_moves, color);
//       let interesting_moves = heuristics::interesting_moves(goban, &legal_moves, color, 10, rng);
//       moves.extend_from_slice(&sample(rng, interesting_moves, 15));
//       for coord in moves {
//         let mut new_goban = goban.clone();
//         new_goban.push_move(coord.as_move(color));
//         if depth > 1 {
//           let (new_value, new_move) = alpha_beta(&mut new_goban, depth - 1, alpha, beta, color.other(), rng);
//           if best_value < new_value {
//             best_value = new_value;
//             best_move = new_move;
//           }
//           alpha = f32::max(alpha, best_value);
//           if beta <= alpha {
//             break;
//           }
//         } else {
//           let mut b_infl_map = [0f32; 361];
//           let b_infl = heuristics::flood_influence(&mut new_goban, Color::Black, &mut b_infl_map, None);
//           let mut w_infl_map = [0f32; 361];
//           let w_infl = heuristics::flood_influence(&mut new_goban, Color::White, &mut w_infl_map, None);
//           return (heuristics::aux_score(&mut new_goban, color.other(), &b_infl_map, &w_infl_map), coord);
//         }
//       }
//       (best_value, best_move)
//     }
//     Color::White => {
//       let mut best_value = f32::INFINITY;
//       let mut best_move = Coord(-1, -1);
//       let legal_moves = goban.legal_moves(color);
//       let mut moves = heuristics::important_moves(goban, &legal_moves, color);
//       let interesting_moves = heuristics::interesting_moves(goban, &legal_moves, color, 10, rng);
//       moves.extend_from_slice(&sample(rng, interesting_moves, 15));
//       for coord in moves {
//         let mut new_goban = goban.clone();
//         new_goban.push_move(coord.as_move(color));
//         if depth > 1 {
//           let (new_value, new_move) = alpha_beta(&mut new_goban, depth - 1, alpha, beta, color.other(), rng);
//           if best_value > new_value {
//             best_value = new_value;
//             best_move = new_move;
//           }
//           beta = f32::min(beta, best_value);
//           if beta <= alpha {
//             break;
//           }
//         } else {
//           let mut b_infl_map = [0f32; 361];
//           let b_infl = heuristics::flood_influence(&mut new_goban, Color::Black, &mut b_infl_map, None);
//           let mut w_infl_map = [0f32; 361];
//           let w_infl = heuristics::flood_influence(&mut new_goban, Color::White, &mut w_infl_map, None);
//           return (heuristics::aux_score(&mut new_goban, color.other(), &b_infl_map, &w_infl_map), coord);
//         }
//       }
//       (best_value, best_move)
//     }
//     _ => (0f32, Coord(-1, -1)),
//   }
// }

pub fn negamax<R: Rng>(goban: &mut Goban,
                       depth: usize,
                       mut alpha: f32,
                       mut beta: f32,
                       color: Color,
                       rng: &mut R,
                       mco: Option<&McOwnerStat>) -> (f32, Coord) {
  let mut best_value = f32::NEG_INFINITY;
  let mut best_move = Coord(-1, -1);
  let legal_moves = goban.legal_moves(color);
  let mut moves = Vec::new();
  let interesting_moves = heuristics::interesting_moves(goban, &legal_moves, color, 10, rng, mco);
  let important_moves = heuristics::important_moves(goban, &legal_moves, color);
  moves.extend_from_slice(&sample(rng, interesting_moves, 10));
  moves.extend_from_slice(&sample(rng, important_moves, 10));
  heuristics::order_moves_by_undecidedness(&mut moves, mco.unwrap());
  for coord in moves {
    if !legal_moves.contains(&coord) {
      continue;
    }
    let mut new_goban = goban.clone();
    new_goban.push_move(coord.as_move(color));
    if depth > 1 {
      let (mut new_value, mut new_move) = negamax(&mut new_goban, depth - 1, -beta, -alpha, color.other(), rng, mco);
      new_value = -new_value;
      if best_value < new_value {
        best_value = new_value;
        best_move = new_move;
      }
      alpha = f32::max(alpha, best_value);
      if beta <= alpha {
        break;
      }
    } else {
      let mut b_infl_map = [0f32; 361];
      let b_infl = heuristics::flood_influence(&mut new_goban, Color::Black, &mut b_infl_map, None);
      let mut w_infl_map = [0f32; 361];
      let w_infl = heuristics::flood_influence(&mut new_goban, Color::White, &mut w_infl_map, None);
      return (heuristics::aux_score(&mut new_goban, color.other(), &b_infl_map, &w_infl_map), coord);
    }
  }
  (best_value, best_move)
}

pub fn negascout<R: Rng>(mut goban: &mut Goban,
                         depth: usize,
                         mut alpha: f32,
                         mut beta: f32,
                         color: Color,
                         rng: &mut R,
                         mco: Option<&McOwnerStat>) -> (f32, Coord) {
  if depth == 0 {
    let mut b_infl_map = [0f32; 361];
    let b_infl = heuristics::flood_influence(&mut goban, Color::Black, &mut b_infl_map, None);
    let mut w_infl_map = [0f32; 361];
    let w_infl = heuristics::flood_influence(&mut goban, Color::White, &mut w_infl_map, None);
    return (color.other().sign() * heuristics::aux_score(&mut goban, color.other(), &b_infl_map, &w_infl_map), goban.last_move.unwrap().pos);
  }
  let mut best_move = Coord(-1, -1);
  let legal_moves = goban.legal_moves(color);
  let mut moves = heuristics::important_moves(goban, &legal_moves, color);
  let interesting_moves = heuristics::interesting_moves(goban, &legal_moves, color, 10, rng, mco);
  moves.extend_from_slice(&sample(rng, interesting_moves, 8));
  heuristics::order_moves_by_undecidedness(&mut moves, mco.unwrap());
  let (mut score, mut score_move) = negascout(&mut goban.with_move(moves[0].as_move(color)), depth - 1, -beta, -alpha, color.other(), rng, mco);
  score = -score;
  for coord in &moves[1..] {
    let mut new_goban = goban.with_move(coord.as_move(color));
    let (mut new_value, mut new_move) = negascout(&mut new_goban, depth - 1, -alpha-1.0, -alpha, color.other(), rng, mco);
    new_value = -new_value;
    if alpha < score && score < beta {
      let (mut full_search_value, full_search_move) = negascout(&mut new_goban, depth - 1, -beta, -score, color.other(), rng, mco);
      full_search_value = -full_search_value;
      new_value = full_search_value;
      new_move = full_search_move;
    }
    if alpha < score {
      alpha = score;
      score_move = new_move;
    }
    if beta <= alpha {
      break;
    }
  }
  (alpha, score_move)
}