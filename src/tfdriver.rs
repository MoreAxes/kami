use go::*;

use rand;
use rand::SeedableRng;

use tf::{
  Graph,
  ImportGraphDefOptions,
  Session,
  SessionOptions,
  StepWithGraph,
  Tensor,
  Operation,
};

use std;

pub struct TfDriver {
  pub goban: Goban,
  pub model_file: std::path::PathBuf,
  pub session: Session,
  pub graph: Graph,
  pub input_op: Operation,
  // pub ext_forbidden_op: Operation,
  pub output_op: Operation,
  pub rng: rand::XorShiftRng,
  pub dist: rand::distributions::Exp,
}

impl TfDriver {
  pub fn from_model_file<P: AsRef<std::path::Path>>(path: P) -> Self {
    let graph = {
      let mut g = Graph::new();
      g.import_graph_def(&std::fs::read(&path).unwrap(), &ImportGraphDefOptions::new()).unwrap();
      g
    };

    let result = TfDriver {
      goban: Goban::new(19, 19),
      model_file: path.as_ref().to_path_buf(),
      session: Session::new(&SessionOptions::new(), &graph).unwrap(),
      input_op: graph.operation_by_name_required("input_1").unwrap(),
      // ext_forbidden_op: graph.operation_by_name_required("ext_forbidden").unwrap(),
      output_op: graph.operation_by_name_required("activation_1/Softmax").unwrap(),
      graph: graph,
      rng: rand::XorShiftRng::from_seed(rand::random()),
      dist: rand::distributions::Exp::new(2.0f64.sqrt()),
    };

    eprintln!("Session devices:");
    for device in result.session.device_list().unwrap() {
      eprintln!("{:?}", device);
    }

    result
  }

  pub fn generate_move(&mut self, for_player: Color) -> Option<Coord> {
    use rand::distributions::Distribution;

    if self.goban.consec_passes > 2 {
      return None;
    }

    let input = {
      let v = self.goban.to_8layer::<f32>(for_player, 0.0, 1.0);
      Tensor::new(&[1, 19, 19, 8]).with_values(&v).unwrap()
    };

    let ext_forbidden = {
      let v = self.goban.get_forbidden::<f32>(for_player, 0.0, 1.0);
      Tensor::new(&[19, 19]).with_values(&v).unwrap()
    };

    let mut step = StepWithGraph::new();
    step.add_input(&self.input_op, 0, &input);
    // step.add_input(&self.ext_forbidden_op, 0, &ext_forbidden);
    let out = step.request_output(&self.output_op, 0);
    self.session.run(&mut step).unwrap();
    let out_res: Tensor<f32> = step.take_output(out).unwrap();

    let mut moves = out_res.iter()
        .enumerate()
        .map(|(it, v)| (Coord((it % 19) as i8, (it / 19) as i8), v))
        .filter(|&(c, _)| self.goban.move_is_legal(Move::new(for_player, c)) && !self.goban.is_simple_eye_for(for_player, c))
        .collect::<Vec<_>>();
    moves.sort_by(|(_, a), (_, b)|
        b.partial_cmp(a).unwrap_or(std::cmp::Ordering::Equal));
    let idx = self.dist.sample(&mut self.rng) as usize;
    if !moves.is_empty() {
      let (c, v) = moves[idx % (3.min(moves.len()))];
      if self.goban.stone_parents.is_empty() || *moves[0].1 > 0.1 / self.goban.stone_parents.len() as f32 {
        Some(c)
      } else {
        None
      }
    } else {
      None
    }
  }
}