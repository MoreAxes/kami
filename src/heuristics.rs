use go::*;
use std::f32;
use std::collections::HashSet;
use std::iter::{FromIterator, Extend};
use std::io::{Write, stderr};

use rand::Rng;
use rand::distributions;
use rand::distributions::IndependentSample;

pub fn aux_score(goban: &mut Goban, color: Color, b_infl_map: &[f32], w_infl_map: &[f32]) -> f32 {
  b_infl_map.iter().zip(w_infl_map.iter()).fold(0f32, |acc, (&b, &w)| acc + {
    let raw = quantize(rectify(b - w));
    // there is more to gain from claiming uncontested territory than contesting
    // enemy territory
    if raw.signum() == color.sign() {
      raw * 2.0
    } else {
      raw
    }
  })
  + nonterritorial_score(goban)
}

pub fn nonterritorial_score(goban: &mut Goban) -> f32 {
  goban.captured_stones[Color::Black as usize] as f32
  - goban.captured_stones[Color::White as usize] as f32
  - goban.stones_in_atari_for(Color::Black) as f32
  + goban.stones_in_atari_for(Color::White) as f32
  - goban.stones_one_move_to_atari_for(Color::Black) as f32 * 0.5
  + goban.stones_one_move_to_atari_for(Color::White) as f32 * 0.5
  - goban.stones_in_suicide_for(Color::Black) as f32
  + goban.stones_in_suicide_for(Color::White) as f32
  - goban.cutting_points_for(Color::Black).len() as f32 * 0.25
  + goban.cutting_points_for(Color::White).len() as f32 * 0.25
  - 6.5
}

static QUANTIZATION_ZERO_WINDOW_RADIUS: f32 = 1.0 / 16.0;
static RECTIFICATION_AMPLIFIER: f32 = 2.8854;

#[inline(always)]
pub fn quantize(x: f32) -> f32 {
  quantize_with(x, QUANTIZATION_ZERO_WINDOW_RADIUS)
}

#[inline(always)]
pub fn quantize_with(x: f32, zero_window_radius: f32) -> f32 {
  if x < -zero_window_radius {
    -1.0
  } else if x < zero_window_radius {
    0.0
  } else {
    1.0
  }
}

pub fn important_moves(goban: &mut Goban, legal_moves: &Vec<Coord>, color: Color) -> Vec<Coord> {
  let mut result = Vec::new();

  // Examine whether taking the ko is worthwhile.
  if let Some(ko_move) = goban.ko_point {
    if ko_move.color != color {
      result.push(ko_move.pos);
    }
  }

  // Examine ate moves.
  goban.atari_points(&mut result);

  result.extend_from_slice(&goban.cutting_points_for(color));
  result.extend_from_slice(&goban.cutting_points_for(color.other()));

  // Examine if a group is worth putting in atari, ot groups one move away from
  // atari are worth extending from.
  goban.one_to_atari_points(&mut result);
  
  // Opponent's suicide points may lead to valuable subtrees.
  result.extend(goban.suicide_points[color.other() as usize].iter());

  // Dedup.
  let mut it = 0;
  while it < result.len() {
    let mut jt = it + 1;
    while jt < result.len() {
      while jt < result.len() && result[it] == result[jt] {
        result.remove(jt);
      }
      jt += 1;
    }
    it += 1;
  }

  result.retain(|c| legal_moves.contains(c));

  result
}

pub fn interesting_moves<R: Rng>(goban: &mut Goban, all_legal_moves: &Vec<Coord>, color: Color, additional: usize, rng: &mut R, mco: Option<&McOwnerStat>) -> Vec<Coord> {
  use std::usize;
  use std::cmp::{PartialOrd, min};

  let mut result = Vec::with_capacity(361);
  result.resize(361, Coord(-1, -1));

  // Examine all liberties, in ascending order of the total number of liberties
  // owned by the chain they belong to (regardless of color)
  let mut chains: Vec<Coord> = goban.chain_cores.keys().cloned().collect();
  chains.sort_by_key(|ch| goban.chain_liberties[ch]);
  let mut start = 0;
  for &ch in &chains {
    start = goban.liberties_of(ch, &mut result[start..]);
  }
  result.resize(start, Coord(-1, -1));

  // // Various approaches to/extensions from single stones
  for &ch in chains.iter().filter(|&&ch| goban.chain_length(ch) == 1) {
    for &pt in ch.explode().iter() {
      result.push(pt);
    }
  }

  // Tournament-select some additional promising moves
  let fuseki_heights = [2, 3, 4];
  let legal_fuseki_moves = all_legal_moves.iter().cloned().filter(|c| {
    let dist = c.rect_distance(Coord(goban.width as i8 / 2, goban.height as i8 / 2));
    let move_height = min(goban.width / 2 - dist, goban.height / 2 - dist);
    fuseki_heights.contains(&move_height)
  }).collect::<Vec<_>>();

  let legal_moves = if goban.stone_parents.len() <= 8 && !legal_fuseki_moves.is_empty() {
    &legal_fuseki_moves
  } else {
    all_legal_moves
  };

  if let Some(mco) = mco {
    result.extend_from_slice(&critical_points(goban.width as usize, goban.height as usize, mco, additional - additional/2, None, rng));
    result.extend_from_slice(&undecided_points(goban.width as usize, goban.height as usize, mco, additional/2, None, rng));
  }

  let range = distributions::Range::new(0usize, legal_moves.len());
  let mut tournament = [usize::MAX; 15];
  for _ in 0..additional {
    for cand in tournament.iter_mut() {
      *cand = range.ind_sample(rng);
    }
    let mut scratch = [0.0f32; 361];
    tournament.sort_by(|&a, &b| {
      // let at_a = (1.0 - rectify(b_infl_map[a] - w_infl_map[a])) * influence_efficiency(goban, Coord::from_lin(a, goban.width as usize, goban.height as usize));
      // let at_b = (1.0 - rectify(b_infl_map[b] - w_infl_map[b])) * influence_efficiency(goban, Coord::from_lin(b, goban.width as usize, goban.height as usize));
      let at_a = flood_influence(goban, color, &mut scratch, Some(legal_moves[a]));
      let at_b = flood_influence(goban, color, &mut scratch, Some(legal_moves[b]));
      f32::partial_cmp(&(at_a*at_a), &(at_b*at_b)).unwrap()
    });
    result.push(legal_moves[*tournament.last().unwrap()]);
  }


  // This is an "expensive" O(n^2) dedup, but it's the only one that keeps the
  // ordering of the array being deduplicated
  let mut it = 0;
  while it < result.len() {
    let mut jt = it + 1;
    while jt < result.len() {
      while jt < result.len() && result[it] == result[jt] {
        result.remove(jt);
      }
      jt += 1;
    }
    it += 1;
  }

  result.retain(|c| all_legal_moves.contains(c));

  result
}

pub fn simple_influence(goban: &mut Goban) -> f32 {
  let mut black_infl = Vec::with_capacity(goban.width as usize * goban.height as usize);
  let mut white_infl = Vec::with_capacity(goban.width as usize * goban.height as usize);
  while black_infl.len() < black_infl.capacity() {
    black_infl.push(0f32);
  }
  while white_infl.len() < white_infl.capacity() {
    white_infl.push(0f32);
  }

  let total_black_infl = influence_map(goban, Color::Black, 1.0, &mut black_infl);
  let total_white_infl = influence_map(goban, Color::White, 1.0, &mut white_infl);

  total_black_infl - total_white_infl
}

pub fn aji(b_infl_map: &[f32], w_infl_map: &[f32]) -> f32 {
  // b_infl_map.iter().zip(w_infl_map.iter()).fold(0f32, |acc, (&b, &w)| acc + aji_for(rectify(b), rectify(w)))
  b_infl_map.iter().zip(w_infl_map.iter()).fold(0f32, |acc, (&b, &w)| acc + rectify(b) * rectify(w))
}

#[inline(always)]
pub fn aji_for(b: f32, w: f32) -> f32 {
  ((b + w)/2.0).sqrt() * (1.0 - ((b-1.0).powf(4.0) + (w-1.0).powf(4.0))/2.0)
}

pub fn influence_map(goban: &mut Goban, color: Color, sign: f32, out_map: &mut [f32]) -> f32 {
  let stones = goban.stones();
  for row in 0..goban.height {
    for col in 0..goban.width {
      let pt = Coord(col as i8, row as i8);
      let infl = stones
                  .iter()
                  .filter(|st| st.color == color && Coord::distance(pt, st.pos) < 5)
                  .map(|&st| simple_influence_for_stone(goban, st, pt) * sign)
                  .fold(0f32, |acc, new| acc + new);
      out_map[pt.to_lin(goban.width as usize, goban.height as usize)] = rectify(infl);
    }
  }

  out_map.iter().fold(0f32, |acc, new| acc + new)
}

#[inline(always)]
pub fn rectify(t: f32) -> f32 {
  // RECTIFICATION_AMPLIFIER is a constant chosen in such a way that rectify(t)
  // is tangent to f(t) = t at t = 0
  2.0 / (1.0 + (-RECTIFICATION_AMPLIFIER * t).exp2()) - 1.0
}

pub fn flood_influence(goban: &mut Goban, color: Color, out_map: &mut [f32], around: Option<Coord>) -> f32 {
  let all_stones = goban.stone_parents.keys().cloned().collect::<Vec<Coord>>();
  let mut stones = HashSet::with_hasher(BuildNoopHasher::new());
  stones.extend(all_stones.iter().cloned().filter(|&st| goban.color_at(st) == color));
  let mut obstacles = HashSet::with_hasher(BuildNoopHasher::new());
  obstacles.extend(all_stones.iter().cloned().filter(|&st| goban.color_at(st) == color.other()));
  let last_move = goban.last_move;
  let goban_width = goban.width;
  let goban_height = goban.height;
  let generating_stones = if let Some(c) = around {
    vec![c]
  } else {
    all_stones
  };
  let mut cursors = generating_stones
      .iter().cloned()
      .filter(|&st| color == Color::Empty || (goban.color_at(st) == color &&
        // Stones in atari generate no influence.
        {
          let core = goban.core_of(st);
          goban.chain_liberties[&core] > 1
        })
      )
      .map(|st| (st, st, 0usize, 1.0 + influence_efficiency(goban_width, goban_height, st)))
      .collect::<Vec<_>>();
  let mut next_cursors = Vec::with_capacity(4 * cursors.len());
  while cursors.len() > 0 {
    for (c, from, age, val) in cursors {
      next_cursors.extend(Liberties::all_at(c, goban.width, goban.height)
                          .points(c)
                          .iter()
                          .filter(|opt| opt.is_some())
                          .map(|opt| opt.unwrap())
                          .map(|st| (st,
                                     c,
                                     age + 1,
                                     val *
                                     0.25 *
                                     (1.0 - influence_eff_delta(st, from, goban.width, goban.height)) *
                                     if obstacles.contains(&st) { 0.5 } else { 1.0 } *
                                     if stones.contains(&st) { 2.0 } else { 1.0 }
                                    )
                              )
                          .filter(|&(st, _, age, hval)| age < 7 && hval > 1.0 / 64.0));
      if !stones.contains(&c) {
        out_map[c.to_lin(goban.width as usize, goban.height as usize)] += val;
      }
    }
    cursors = next_cursors;
    next_cursors = Vec::with_capacity(4 * cursors.len());
  }
  
  let mut total = 0f32;
  for pt in out_map.iter() {
    total += rectify(*pt);
  }
  total
}

#[inline(always)]
pub fn influence_eff_delta(at: Coord, from: Coord, width: u8, height: u8) -> f32 {
  influence_efficiency(width, height, at) - influence_efficiency(width, height, from)
}

#[inline(always)]
fn clamp(this: f32, min: f32, max: f32) -> f32 {
  if this < min {
    min
  } else if this > max {
    max
  } else {
    this
  }
}

pub fn simple_influence_around(goban: &Goban, st: Move, radius: u8, b_infl_map: &[f32], w_infl_map: &[f32]) -> f32 {
  let mut total = 0.0;
  for it in 0..(goban.width as usize * goban.height as usize) {
    let current = Coord::from_lin(it, goban.width as usize, goban.height as usize);
    if st.pos.distance(current) <= radius {
      total += simple_influence_for_stone(goban, st, current) * (1.0 - rectify(b_infl_map[it] - w_infl_map[it]).abs())
    }
  }
  total
}

pub fn simple_influence_for_stone(goban: &Goban, st: Move, at: Coord) -> f32 {
  if st.pos == at {
    return 0f32;
  }
  f32::exp2(-(Coord::distance(at, st.pos) as f32 - 1.0)) *
  influence_efficiency(goban.width, goban.height, at)
}

static MAX_INFL_EFFICIENCY_DIST: f32 = 2.0 / 3.0;
static INFL_FALLOFF_RATE: f32 = 3.0 / 1.414;

pub fn influence_efficiency(width: u8, height: u8, at: Coord) -> f32 {
  let middle = Coord(width as i8 / 2, height as i8 / 2);
  let dist = Coord::euclidean_distance(at, middle) / (width / 2) as f32;

  let eff = dist / (MAX_INFL_EFFICIENCY_DIST + 3.0 / width as f32);

  let result = f32::max(0.5, if eff <= 1.0 {
    eff
  } else {
    2.0 / INFL_FALLOFF_RATE + INFL_FALLOFF_RATE * (1.0 - eff)
  });

  result * result
}

pub fn mc_playout<R: Rng>(mut goban: Goban,
                          first_move: Move,
                          mut depth: usize,
                          b_ownership: &mut [u8],
                          w_ownership: &mut [u8],
                          winner_ownership: &mut [u8],
                          rng: &mut R) -> Color {
  // first_move is assumed to be already legal
  let _ = goban.push_move(first_move);

  let mut to_play = first_move.color.other();
  while !goban.game_has_ended() && depth > 0 {
    let mut legal_moves = goban.legal_moves(to_play);
    legal_moves.retain(|&c| !goban.is_simple_eye_for(to_play, c));
    let mov = match rng.choose(&legal_moves) {
      Some(&c) => c.as_move(to_play),
      None => Move::pass(),
    };
    goban.push_move(mov);
    to_play.flip();
    depth -= 1;
  }

  let mut b_infl_map = [0f32; 361];
  let b_infl = flood_influence(&mut goban, Color::Black, &mut b_infl_map, None);
  let mut w_infl_map = [0f32; 361];
  let w_infl = flood_influence(&mut goban, Color::White, &mut w_infl_map, None);

  let mut total_infl = 0.0;
  let mut infl_map = Vec::from_iter(b_infl_map.iter().zip(w_infl_map.iter())
      .map(|(b, w)| {
        let infl = quantize(rectify(b - w));
        total_infl += infl;
        infl
      }));
  let score = total_infl + nonterritorial_score(&mut goban);
  let winner = if score < 0.0 { Color::White } else { Color::Black };

  for it in 0..(goban.width as usize * goban.height as usize) {
    let q = infl_map[it];
    if q < 0.0 {
      w_ownership[it] += 1;
      if winner == Color::White {
        winner_ownership[it] += 1;
      }
    } else if q > 0.0 {
      b_ownership[it] += 1;
      if winner == Color::Black {
        winner_ownership[it] += 1;
      }
    }
  }

  winner
}

pub struct McOwnerStat {
  pub width: usize,
  pub height: usize,
  pub playouts: usize,
  pub b_wins: u8,
  pub w_wins: u8,
  pub black: Vec<u8>,
  pub white: Vec<u8>,
  pub winner: Vec<u8>,
}

pub fn mc_owner<R: Rng>(root: &mut Goban,
                        playouts: usize,
                        to_play: Color,
                        previous: Option<&McOwnerStat>,
                        rng: &mut R) -> Option<McOwnerStat> {
  let intersections = root.intersections();
  let mut b_ownership = Vec::with_capacity(intersections);
  b_ownership.resize(intersections, 0);
  let mut w_ownership = Vec::with_capacity(intersections);
  w_ownership.resize(intersections, 0);
  let mut winner_ownership = Vec::with_capacity(intersections);
  winner_ownership.resize(intersections, 0);

  let legal_moves = root.legal_moves(to_play);
  if legal_moves.is_empty() {
    return None;
  }

  let mut b_wins = 0;
  let mut w_wins = 0;

  for _ in 0..playouts {
    let mov = if let Some(mco) = previous {
      let mut sample = Vec::with_capacity(10);
      sample.extend_from_slice(&critical_points(root.width as usize, root.height as usize, mco, 5, None, rng));
      sample.extend_from_slice(&undecided_points(root.width as usize, root.height as usize, mco, 5, None, rng));
      rng.choose(&sample).unwrap().as_move(to_play)
    } else {
      rng.choose(&legal_moves).unwrap().as_move(to_play)
    };
    let winner = mc_playout(root.clone(),
                            rng.choose(&legal_moves).unwrap().as_move(to_play),
                            250,
                            &mut b_ownership,
                            &mut w_ownership,
                            &mut winner_ownership,
                            rng);
    if winner == Color::Black {
      b_wins += 1;
    } else {
      w_wins += 1;
    }
  }

  Some(McOwnerStat {
    width: root.width as usize,
    height: root.height as usize,
    playouts: playouts,
    b_wins: b_wins,
    w_wins: w_wins,
    black: b_ownership,
    white: w_ownership,
    winner: winner_ownership,
  })
}

pub fn chained_mc_owner<R: Rng>(root: &mut Goban,
                                rounds: usize,
                                playouts_per_round: usize,
                                to_play: Color,
                                rng: &mut R) -> Option<McOwnerStat> {
  let mut mco = mc_owner(root, playouts_per_round, to_play, None, rng);
  for _ in 1..rounds {
    mco = mc_owner(root, playouts_per_round, to_play, mco.as_ref(), rng);
  }
  mco
}

pub fn undecided_points<R: Rng>(width: usize,
                                height: usize,
                                mco: &McOwnerStat,
                                sample_size: usize,
                                legal_moves: Option<&[Coord]>,
                                rng: &mut R) -> Vec<Coord> {
  let mut result = Vec::with_capacity(sample_size);

  let range = distributions::Range::new(0usize, mco.black.len());
  for _ in 0..sample_size {
    let mut tournament = [0usize; 15];
    for cand in tournament.iter_mut() {
      *cand = range.ind_sample(rng);
    }
    match tournament.iter().min_by_key(|&&it| (mco.black[it] as i8 - mco.white[it] as i8).abs()) {
      Some(&it) => result.push(Coord::from_lin(it, width, height)),
      None => {},
    }
  }

  if let Some(moves) = legal_moves {
    result.retain(|c| moves.contains(&c));
  }

  result
}

pub fn critical_points<R: Rng>(width: usize,
                               height: usize,
                               mco: &McOwnerStat,
                               sample_size: usize,
                               legal_moves: Option<&[Coord]>,
                               rng: &mut R) -> Vec<Coord> {
  let mut result = Vec::with_capacity(sample_size);

  let range = distributions::Range::new(0usize, mco.black.len());
  for _ in 0..sample_size {
    let mut tournament = [0usize; 15];
    for cand in tournament.iter_mut() {
      *cand = range.ind_sample(rng);
    }
    match tournament.iter().max_by_key(|&&it| {
        let b_wins_there = mco.black[it] as f32;
        let w_wins_there = mco.white[it] as f32;
        let winners_wins_there = mco.winner[it] as f32;
        let n = mco.playouts as f32;
        ((winners_wins_there / n - (b_wins_there / n * mco.b_wins as f32 + w_wins_there / n * mco.w_wins as f32)) * 1024.0) as usize
      }) {
      Some(&it) => result.push(Coord::from_lin(it, width, height)),
      None => {},
    }
  }

  if let Some(moves) = legal_moves {
    result.retain(|c| moves.contains(&c));
  }

  result
}

pub fn order_moves_by_criticality(moves: &mut [Coord], mco: &McOwnerStat) {
  moves.sort_by_key(|m| -{
    let b_wins_there = mco.black[m.to_lin(mco.width, mco.height)] as f32;
    let w_wins_there = mco.white[m.to_lin(mco.width, mco.height)] as f32;
    let winners_wins_there = mco.winner[m.to_lin(mco.width, mco.height)] as f32;
    let n = mco.playouts as f32;
    ((winners_wins_there / n - (b_wins_there / n * mco.b_wins as f32 + w_wins_there / n * mco.w_wins as f32)) * 1024.0) as isize
  })
}

pub fn order_moves_by_undecidedness(moves: &mut [Coord], mco: &McOwnerStat) {
  moves.sort_by_key(|m| -{
    (mco.black[m.to_lin(mco.width, mco.height)] as i8 - mco.white[m.to_lin(mco.width, mco.height)] as i8).abs()
  })
}