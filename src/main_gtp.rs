#![allow(dead_code)]
#![allow(unused_variables)]

#![feature(core_intrinsics)]
#![feature(proc_macro)]
#![feature(zero_one)]

extern crate rand;
extern crate gtprust as gr;
extern crate tensorflow as tf;
extern crate clap;
extern crate thunder;

mod go;
mod gtp;
mod tfdriver;

use tfdriver::TfDriver;

use thunder::thunderclap;

struct TfDriverHost;

#[thunderclap]
impl TfDriverHost {
  fn host(model_file: &str) {
    let mut bot = TfDriver::from_model_file(model_file);
    gr::main_loop(&mut bot);
  }
}

fn main() {
  TfDriverHost::start();
}