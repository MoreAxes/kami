use go::*;
use tfdriver::*;

use gr::api;

// use rand::{random, SeedableRng, XorShiftRng};

use std::usize;

impl api::Gtp for TfDriver {
  fn name(&self) -> String {
    "kami".to_string()
  }

  fn version(&self) -> String {
    "v0.0.1".to_string()
  }

  fn clear_board(&mut self) {
    *self = TfDriver::from_model_file(&self.model_file);
  }

  fn komi(&mut self, komi: f32) {
  }

  fn boardsize(&mut self, size: usize) -> Result<(), api::GTPError> {
    if size != 19 {
      Err(api::GTPError::InvalidBoardSize)
    } else {
      self.clear_board();
      Ok(())
    }
  }

  fn play(&mut self, mov: api::ColouredMove) -> Result<(), api::GTPError> {
    use std::io::{stderr, Write};

    let mut col = match mov.player {
      api::Colour::Black => Color::Black,
      api::Colour::White => Color::White,
    };
    let pos = match mov.mov {
      api::Move::Stone(vert) => {
        let (x, y) = vert.to_coords();
        Coord(x as i8, y as i8)
      }
      api::Move::Pass | api::Move::Resign => {
        col = Color::Empty;
        Coord(-1, -1)
      },
    };
    // offset by (-1, -1) due to GTP requirements
    match self.goban.push_move(Move::new(col, pos + (-1, -1))) {
      Err(_) => Err(api::GTPError::InvalidMove),
      Ok(_) => {
        let _ = writeln!(stderr(), "{}", self.goban.repr(true, true));
        Ok(())
      },
    }
  }

  fn genmove(&mut self, player: api::Colour) -> api::Move {
    use std::io::{stderr, Write};

    let col = match player {
      api::Colour::Black => Color::Black,
      api::Colour::White => Color::White,
    };
    // let mut rng = XorShiftRng::from_seed(random());
    let mv = self.generate_move(col);
    match mv {
      Some(pos) => {
        self.goban.push_move(Move::new(col, pos)).unwrap();
        let _ = writeln!(stderr(), "{}", self.goban.repr(true, true));
        api::Move::Stone(api::Vertex::from_coords(pos.col() as u8 + 1, pos.row() as u8 + 1).unwrap())
      },
      None => api::Move::Pass,
    }
  }

  fn custom_command(&mut self, command: &str, args: &str) -> (bool, String) {
    use std::io::{stderr, Write};
    let _ = writeln!(stderr(), "{}", self.goban.repr(true, true));
    (true, "".to_string())
  }
}
