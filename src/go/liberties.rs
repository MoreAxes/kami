use std::fmt;
use std::ops::{Add, Sub};

use go::*;

#[derive(Clone, Copy, PartialEq, Eq)]
pub struct Liberties {
  pub flags: u8,
}

impl Liberties {
  pub fn all() -> Liberties {
    use go::Dir::*;
    Liberties { flags: Up as u8 | Right as u8 | Down as u8 | Left as u8 }
  }

  pub fn all_at(at: Coord, width: u8, height: u8) -> Liberties {
    let mut result = Liberties::all();
    // This needs to use fully qualified method names due to confusion with the
    // Add<Dir> impl below.
    if (at.row() as u8) == 0 {
      Liberties::remove(&mut result, Dir::Up);
    }
    if (at.col() as u8) == 0 {
      Liberties::remove(&mut result, Dir::Left);
    }
    if (at.row() as u8) == height - 1 {
      Liberties::remove(&mut result, Dir::Down);
    }
    if (at.col() as u8) == width - 1 {
      Liberties::remove(&mut result, Dir::Right);
    }
    result
  }

  pub fn none() -> Liberties {
    Liberties { flags: 0 }
  }

  pub fn has(&self, dir: Dir) -> bool {
    self.flags & dir as u8 > 0
  }

  pub fn count(&self) -> u8 {
    use std::intrinsics::ctpop;
    unsafe { ctpop(self.flags) }
  }

  pub fn add(&mut self, dir: Dir) -> &mut Liberties {
    self.flags |= dir as u8;
    self
  }

  pub fn added(&self, dir: Dir) -> Liberties {
    let mut cp = self.clone();
    *(Liberties::add(&mut cp, dir))
  }

  pub fn remove(&mut self, dir: Dir) -> &mut Liberties {
    self.flags &= !(dir as u8);
    self
  }

  pub fn removed(&self, dir: Dir) -> Liberties {
    let mut cp = self.clone();
    *(Liberties::remove(&mut cp, dir))
  }

  pub fn points(&self, at: Coord) -> [Option<Coord>; 4] {
    [if self.has(Dir::Up) {
       Some(at + (0, -1))
     } else {
       None
     },
     if self.has(Dir::Right) {
       Some(at + (1, 0))
     } else {
       None
     },
     if self.has(Dir::Down) {
       Some(at + (0, 1))
     } else {
       None
     },
     if self.has(Dir::Left) {
       Some(at + (-1, 0))
     } else {
       None
     }]
  }
}

impl fmt::Debug for Liberties {
  #[allow(unused_must_use)]
  fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
    use std::fmt::Write;

    f.write_char('[');
    if self.has(Dir::Up) {
      f.write_char('u');
    }
    if self.has(Dir::Right) {
      f.write_char('r');
    }
    if self.has(Dir::Down) {
      f.write_char('d');
    }
    if self.has(Dir::Left) {
      f.write_char('l');
    }
    f.write_char(']');

    Ok(())
  }
}

impl fmt::Display for Liberties {
  #[allow(unused_must_use)]
  fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
    use std::fmt::Write;

    f.write_char('[');
    if self.has(Dir::Up) {
      f.write_char('u');
    }
    if self.has(Dir::Right) {
      f.write_char('r');
    }
    if self.has(Dir::Down) {
      f.write_char('d');
    }
    if self.has(Dir::Left) {
      f.write_char('l');
    }
    f.write_char(']');

    Ok(())
  }
}

impl Add<Dir> for Liberties {
  type Output = Liberties;

  fn add(self, rhs: Dir) -> Liberties {
    self.added(rhs)
  }
}

impl Sub<Dir> for Liberties {
  type Output = Liberties;

  fn sub(self, rhs: Dir) -> Liberties {
    self.removed(rhs)
  }
}
