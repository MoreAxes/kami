mod color;
mod coord;
mod goban;
mod liberties;
mod mov;
mod noop_hash;

pub use self::color::*;
pub use self::coord::*;
pub use self::goban::*;
pub use self::liberties::*;
pub use self::mov::*;
pub use self::noop_hash::*;

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum Dir {
  Up = 1,
  Right = 2,
  Down = 4,
  Left = 8,
}

pub fn draw_points(width: i8, height: i8, with_indicators: bool, points: &[Coord], pen: char) -> String {
  let mut result = String::with_capacity(2 * width as usize * (height as usize + 1));
  if with_indicators {
    result.push_str("A B C D E F G H J K L M N O P Q R S T\n");
  }
  for row in 0..height {
    for col in 0..width {
      let fallback = if (row == 3 || row == 9 || row == 15) && (col == 3 || col == 9 || col == 15) {
        '*'
      } else {
        '.'
      };
      result.push(if points.contains(&Coord(row, col)) {
        pen
      } else {
        fallback
      });
      result.push(' ');
    }
    if with_indicators {
      result.push_str(&format!("{}", row + 1));
    }
    result.push('\n');
  }
  result
}
