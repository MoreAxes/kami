use go::*;

use std::fmt;

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub struct Move {
  pub color: Color,
  pub pos: Coord,
}

impl Move {
  pub fn new(color: Color, pos: Coord) -> Move {
    Move {
      color: color,
      pos: pos,
    }
  }

  pub fn black(row: i8, col: i8) -> Move {
    Move::new(Color::Black, Coord(row, col))
  }

  pub fn white(row: i8, col: i8) -> Move {
    Move::new(Color::White, Coord(row, col))
  }

  pub fn pass() -> Move {
    Move {
      color: Color::Empty,
      pos: Coord(-1, -1),
    }
  }
}

impl fmt::Display for Move {
  fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
    write!(f, "{:?} {}", self.color, self.pos.to_gtp())
  }
}