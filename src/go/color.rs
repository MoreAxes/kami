#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub enum Color {
  Black,
  White,
  Empty,
}

impl Color {
  pub fn other(&self) -> Color {
    match self {
      &Color::Black => Color::White,
      &Color::White => Color::Black,
      &Color::Empty => Color::Empty,
    }
  }

  pub fn flip(&mut self) {
    *self = self.other();
  }

  pub fn sign(&self) -> f32 {
    use std::f32;
    match *self {
      Color::Black => 1.0,
      Color::White => -1.0,
      Color::Empty => f32::NAN,
    }
  }
}
