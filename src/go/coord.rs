use go::{Color, Move};

use std::fmt;
use std::ops::{Add, Sub};

#[derive(Clone, Copy, PartialEq, Eq, Hash, PartialOrd, Ord)]
pub struct Coord(pub i8, pub i8);

impl fmt::Debug for Coord {
  fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
    write!(f, "({}, {})", self.row(), self.col())
  }
}

impl fmt::Display for Coord {
  fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
    write!(f, "({}, {})", self.row(), self.col())
  }
}


impl Coord {
  pub fn col(&self) -> i8 {
    self.0
  }
  pub fn row(&self) -> i8 {
    self.1
  }

  pub fn fits_in(&self, width: i8, height: i8) -> bool {
    self.col() >= 0 && self.row() >= 0 && self.col() < width && self.row() < height
  }

  pub fn from_gtp(gtp: &str) -> Coord {
    use std::str::FromStr;
    let col: usize = "abcdefghjklmnopqrst"
                .find(gtp.chars().nth(0).unwrap().to_ascii_lowercase()).unwrap();
    let row = i8::from_str(&gtp[1..]).unwrap() - 1;
    Coord(row, col as i8)
  }

  pub fn to_gtp(&self) -> String {
    let letter = "abcdefghjklmnopqrst".chars().nth(self.col() as usize).unwrap();
    format!("{}{}", letter, self.row())
  }

  pub fn transposed(&self) -> Coord {
    Coord(self.1, self.0)
  }

  pub fn to_lin(&self, width: usize, height: usize) -> usize {
    self.row() as usize * width + self.col() as usize
  }

  pub fn from_lin(lin: usize, width: usize, height: usize) -> Coord {
    Coord((lin / width) as i8, (lin % width) as i8)
  }

  pub fn distance(self, c2: Coord) -> u8 {
    ((self.col() - c2.col()).abs() + (self.row() - c2.row()).abs()) as u8
  }

  pub fn euclidean_distance(self, c2: Coord) -> f32 {
    let dx = (self.col() - c2.col()) as f32;
    let dy = (self.row() - c2.row()) as f32;
    (dx * dx + dy * dy).sqrt()
  }

  pub fn rect_distance(self, c2: Coord) -> u8 {
    use std::cmp::max;
    let dx = (self.col() as i16 - c2.col() as i16).abs();
    let dy = (self.row() as i16 - c2.row() as i16).abs();
    max(dx, dy) as u8
  }

  pub fn as_move(&self, col: Color) -> Move {
    Move::new(col, *self)
  }

  pub fn dilate(&self) -> [Coord; 5] {
    let mut result = [*self; 5];
    result[1] = *self + (0, 1);
    result[2] = *self + (1, 0);
    result[3] = *self + (-1, 0);
    result[4] = *self + (0, -1);
    result
  }

  pub fn neighbours(&self) -> [Coord; 4] {
    let mut result = [*self; 4];
    result[0] = *self + (0, 1);
    result[1] = *self + (1, 0);
    result[2] = *self + (-1, 0);
    result[3] = *self + (0, -1);
    result
  }

  // this is essentially a double dilate() + all knight's moves, with the 
  // interior removed
  pub fn explode(&self) -> [Coord; 16] {
    let mut result = [*self; 16];

    // One point jumps
    result[0] = *self + (0, 2);
    result[1] = *self + (2, 0);
    result[2] = *self + (-2, 0);
    result[3] = *self + (0, -2);

    // Keimas
    result[4] = *self + (1, 2);
    result[5] = *self + (2, -1);
    result[6] = *self + (-1, -2);
    result[7] = *self + (-2, 1);

    result[8] = *self + (-1, 2);
    result[9] = *self + (2, 1);
    result[10] = *self + (1, -2);
    result[11] = *self + (-2, -1);

    // Kosumi
    result[12] = *self + (1, 1);
    result[13] = *self + (1, -1);
    result[14] = *self + (-1, -1);
    result[15] = *self + (-1, 1);

    result
  }
}

impl Add<(i8, i8)> for Coord {
  type Output = Coord;
  fn add(self, rhs: (i8, i8)) -> Coord {
    Coord(self.0 + rhs.0, self.1 + rhs.1)
  }
}

impl Sub<Coord> for Coord {
  type Output = (i8, i8);
  fn sub(self, rhs: Coord) -> (i8, i8) {
    (self.col() - rhs.col(), self.row() - rhs.row())
  }
}
