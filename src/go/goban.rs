use std::collections::HashMap;
use std::collections::HashSet;

use go::*;

#[derive(Clone, Copy, PartialEq, Eq, Debug)]
pub enum MoveErr {
  OutOfBounds,
  Occupied,
  Suicide,
  Ko,
  GameHasEnded,
}

#[derive(Clone)]
pub struct Goban {
  pub width: u8,
  pub height: u8,
  pub search_depth: usize,
  pub consec_passes: u8,
  pub ko_point: Option<Move>,
  pub last_move: Option<Move>,
  pub captured_stones: [usize; 2],
  pub stone_parents: HashMap<Coord, Coord, BuildNoopHasher>,
  pub chain_cores: HashMap<Coord, Color, BuildNoopHasher>,
  pub chain_liberties: HashMap<Coord, u8, BuildNoopHasher>,
  pub suicide_points: Vec<HashSet<Coord, BuildNoopHasher>>,
}

impl Goban {
  pub fn new(width: u8, height: u8) -> Goban {
    Goban {
      width: width,
      height: height,
      search_depth: 4,
      consec_passes: 0,
      ko_point: None,
      last_move: None,
      captured_stones: [0; 2],
      stone_parents: HashMap::with_hasher(BuildNoopHasher::new()),
      chain_cores: HashMap::with_hasher(BuildNoopHasher::new()),
      chain_liberties: HashMap::with_hasher(BuildNoopHasher::new()),
      suicide_points: vec![HashSet::with_hasher(BuildNoopHasher::new()); 2],
    }
  }

  pub fn repr(&mut self, with_indicators: bool, mark_hoshi: bool) -> String {
    use go::Color::*;
    let mut result = String::with_capacity(2 * self.width as usize * (self.height as usize + 1));
    if with_indicators {
      result.push_str("A B C D E F G H J K L M N O P Q R S T\n");
    }
    for row in 0..self.height {
      for col in 0..self.width {
        let fallback = if mark_hoshi && (row == 3 || row == 9 || row == 15) && (col == 3 || col == 9 || col == 15) {
          '*'
        } else {
          if let &Some(ko) = &self.ko_point {
            if ko.pos == Coord(row as i8, col as i8) {
              '$'
            } else {
              '.'
            }
          } else {
            '.'
          }
        };
        let color = self.color_at(Coord(row as i8, col as i8));
        result.push(match color {
          Empty => fallback,
          Black => 'X',
          White => 'O',
        });
        result.push(' ');
      }
      if with_indicators {
        result.push_str(&format!("{}", row + 1));
      }
      result.push('\n');
    }
    result
  }

  pub fn new_chain(&mut self, coord: Coord, color: Color) {
    self.stone_parents.insert(coord, coord);
    self.chain_cores.insert(coord, color);
  }

  pub fn core_of(&mut self, c: Coord) -> Coord {
    let mut ptr = c;
    while ptr != self.stone_parents[&ptr] {
      ptr = self.stone_parents[&ptr];
    }
    self.stone_parents.insert(c, ptr);
    ptr
  }

  pub fn core_of_with_rank(&mut self, c: Coord) -> (Coord, u16) {
    let mut rank = 0;
    let mut ptr = c;
    while ptr != self.stone_parents[&ptr] {
      ptr = self.stone_parents[&ptr];
      rank += 1;
    }
    self.stone_parents.insert(c, ptr);
    (ptr, rank)
  }

  pub fn join_chains(&mut self, c1: Coord, c2: Coord) {
    let (chain1, rank1) = self.core_of_with_rank(c1);
    let (chain2, rank2) = self.core_of_with_rank(c2);
    if chain1 == chain2 {
      // same set
      return;
    }

    if rank1 < rank2 {
      self.stone_parents.insert(chain1, chain2);
      self.chain_cores.remove(&chain1);
    } else {
      self.stone_parents.insert(chain2, chain1);
      self.chain_cores.remove(&chain2);
    }
  }

  pub fn color_at(&mut self, c: Coord) -> Color {
    if !self.stone_parents.contains_key(&c) {
      return Color::Empty;
    }

    let parent_of_c = self.stone_parents[&c];
    let core_of_parent = self.core_of(parent_of_c);
    self.chain_cores[&core_of_parent]
  }

  pub fn neighbouring_filled_points(&self, c: Coord, out: &mut [Coord]) -> usize {
    let mut kept = 0;
    let point_count = self.neighbouring_points(c, out);
    for it in 0..point_count as usize {
      if self.stone_parents.contains_key(&out[it]) {
        out[kept] = out[it];
        kept += 1;
      }
    }
    kept
  }

  pub fn neighbouring_points(&self, c: Coord, out: &mut [Coord]) -> usize {
    let all: &[Coord] = &[c + (0, -1), c + (1, 0), c + (0, 1), c + (-1, 0)];
    let mut it = 0;
    for &pt in all.iter().filter(|p| p.fits_in(self.width as i8, self.height as i8)) {
      out[it] = pt;
      it += 1;
    }
    it
  }

  pub fn chain_points(&mut self, core: Coord, out: &mut [Coord]) -> usize {
    let mut length = 0;

    // HOT PATH ALLOC
    let all_stones = self.stone_parents.keys().cloned().collect::<Vec<_>>();

    for st in all_stones {
      if self.core_of(st) == core {
        out[length] = st;
        length += 1;
      }
    }
    length
  }

  pub fn chain_length(&mut self, core: Coord) -> usize {
    let mut length = 0;

    // HOT PATH ALLOC
    let all_stones = self.stone_parents.keys().cloned().collect::<Vec<_>>();

    for st in all_stones {
      if self.core_of(st) == core {
        length += 1;
      }
    }
    length
  }

  pub fn contour_of(&mut self, core: Coord, out: &mut [Coord]) -> usize {
    let chain_length = self.chain_points(core, out);
    let mut contour_length = 0;

    // HOT PATH ALLOC
    let mut lib_set = HashSet::with_hasher(BuildNoopHasher::new());

    for &st in out[..chain_length].iter() {
      for &lib in Liberties::all_at(st, self.width, self.height).points(st).iter() {
        if let Some(pt) = lib {
          lib_set.insert(pt);
        }
      }
    }

    for st in out[..chain_length].iter() {
      lib_set.remove(st);
    }

    for pt in lib_set {
      out[contour_length] = pt;
      contour_length += 1;
    }
    contour_length
  }

  pub fn liberties_of(&mut self, core: Coord, out: &mut [Coord]) -> usize {
    let mut buf = [Coord(-1, -1); 361];
    let contour_length = self.contour_of(core, &mut buf);
    let mut liberty_count = 0;

    for it in 0..contour_length {
      // Liberties::points already rejects out-of-bounds points, so we don't
      // need to do it here again
      if !self.stone_parents.contains_key(&buf[it]) {
        out[liberty_count] = buf[it];
        liberty_count += 1;
      }
    }
    liberty_count
  }

  pub fn recalculate_chain_liberties(&mut self, cores: &[Coord]) -> bool {
    let mut killed_chains = false;
    let mut scratch = [Coord(0, 0); 361];
    for &core in cores {
      let liberties_of_core = self.liberties_of(core, &mut scratch);
      self.chain_liberties.insert(core, liberties_of_core as u8);
      killed_chains |= liberties_of_core == 0;
    }
    killed_chains
  }

  pub fn remove_dead_chains(&mut self, col: Color, scratch: &mut [Coord]) -> usize {
    // HOT PATH ALLOC
    let all_stones = self.stone_parents.keys().cloned().collect::<Vec<_>>();
    // HOT PATH ALLOC
    let mut removed_chains = HashSet::with_hasher(BuildNoopHasher::new());

    let mut removed_stones = 0;

    for st in all_stones {
      let core = self.core_of(st);
      if self.chain_cores[&core] == col && self.chain_liberties[&core] == 0 {
        scratch[removed_stones] = st;
        removed_stones += 1;
        removed_chains.insert(core);
      }
    }

    for core in removed_chains {
      self.chain_cores.remove(&core);
      self.chain_liberties.remove(&core);
    }

    for st in &scratch[..removed_stones] {
      self.stone_parents.remove(&st);
    }

    removed_stones
  }

  pub fn recalculate_suicide_points(&mut self, col_to_play: Color, scratch: &mut [Coord]) {
    // A liberty point L is a suicide point for color C under the following
    // conditions (be wary of sufficiency vs. necessity as noted):
    //   (1)  (nec) it's fully surrounded
    //   (2)  (nec) (1) and none of its neighbouring non-C chains are in atari
    //   (3)  (nec) (1) and not (2) and all of its neighbouting C chains are in
    //              atari

    self.suicide_points[col_to_play as usize].clear();

    // scratch - cores of chains in atari
    let mut pts_to_ex = 0;
    for it in 0..self.width {
      for jt in 0..self.height {
        let pt = Coord(it as i8, jt as i8);
        if !self.stone_parents.contains_key(&pt) {
          scratch[pts_to_ex] = pt;
          pts_to_ex += 1;
        }
      }
    }

    let mut sec_scratch = [Coord(-1, -1); 4];
    for &lib in &scratch[..pts_to_ex] {
      let neighbours = self.neighbouring_filled_points(lib, &mut sec_scratch);
      if (neighbours as u8) < Liberties::all_at(lib, self.width, self.height).count() {
        // condition (1) not satisfied
        continue;
      }

      for pt in &mut sec_scratch[..neighbours] {
        *pt = self.core_of(*pt);
      }

      let enemies = sec_scratch[..neighbours].iter().filter(|&&pt| self.color_at(pt) == col_to_play.other()).count();
      let allies = sec_scratch[..neighbours].iter().filter(|&&pt| self.color_at(pt) == col_to_play).count();

      let mut enemies_not_in_atari = 0;
      let mut allies_in_atari = 0;

      for core in sec_scratch[..neighbours].iter() {
        if self.chain_liberties[core] > 1 && self.chain_cores[core] == col_to_play.other() {
          enemies_not_in_atari += 1;
        }

        if self.chain_liberties[core] == 1 && self.chain_cores[core] == col_to_play {
          allies_in_atari += 1;
        }
      }

      if enemies_not_in_atari == enemies && allies_in_atari == allies {
        self.suicide_points[col_to_play as usize].insert(lib);
      }
    }
  }

  pub fn game_has_ended(&self) -> bool {
    self.consec_passes == 2
  }

  pub fn push_move(&mut self, mov: Move) -> Result<(), MoveErr> {
    if let Color::Empty = mov.color {
      self.consec_passes += 1;
      return Ok(());
    }

    if !mov.pos.fits_in(self.width as i8, self.height as i8) {
      return Err(MoveErr::OutOfBounds);
    }

    if self.stone_parents.contains_key(&mov.pos) {
      return Err(MoveErr::Occupied);
    }

    if self.suicide_points[mov.color as usize].contains(&mov.pos) {
      return Err(MoveErr::Suicide);
    }

    if let Some(ko_mov) = self.ko_point {
      if ko_mov == mov {
        return Err(MoveErr::Ko);
      }
    }

    self.last_move = Some(mov);
    self.consec_passes = 0;
    self.ko_point = None;

    self.new_chain(mov.pos, mov.color);

    let mut scratch = [Coord(0, 0); 361];
    let neighbours = self.neighbouring_filled_points(mov.pos, &mut scratch);
    eprintln!("neighbours: {}", neighbours);
    let mut liberties_at_mov = Liberties::all_at(mov.pos, self.width, self.height);
    for st in scratch[..neighbours].iter() {
      match *st - mov.pos {
        (0, 1) => {
          liberties_at_mov.remove(Dir::Down);
        }
        (0, -1) => {
          liberties_at_mov.remove(Dir::Up);
        }
        (1, 0) => {
          liberties_at_mov.remove(Dir::Right);
        }
        (-1, 0) => {
          liberties_at_mov.remove(Dir::Left);
        }
        _ => {}
      }
    }

    let mut made_joins = false;
    for &ally in scratch[..neighbours].iter() {
      if self.color_at(ally) == mov.color {
        self.join_chains(ally, mov.pos);
        made_joins = true;
      }
    }

    for st in &mut scratch[..neighbours] {
      *st = self.core_of(*st);
    }

    scratch[neighbours] = self.core_of(mov.pos);
    scratch[..neighbours+1].sort();
    let mut unique_cores = 0;
    for it in 0..neighbours+1 {
      if scratch[it] != scratch[unique_cores] {
        unique_cores += 1;
        scratch[unique_cores] = scratch[it];
      }
    }
    unique_cores += 1;

    let need_kill_pass = self.recalculate_chain_liberties(&scratch[..unique_cores]);
    if need_kill_pass {
      let killed_stones = self.remove_dead_chains(mov.color.other(), &mut scratch);
      self.captured_stones[mov.color as usize] += killed_stones;
      let mut it = 0;
      for st in self.chain_cores.keys() {
        scratch[it] = *st;
        it += 1;
      }
      scratch[it] = mov.pos;
      let mut scratch_2 = [Coord(0, 0); 361];
      let neighbours = self.neighbouring_filled_points(mov.pos, &mut scratch_2[..]);
      eprintln!("neighbours: {}", neighbours);
      let mut liberties_at_mov = Liberties::all_at(mov.pos, self.width, self.height);
      for st in scratch_2[..neighbours].iter() {
        match *st - mov.pos {
          (0, 1) => {
            liberties_at_mov.remove(Dir::Down);
          }
          (0, -1) => {
            liberties_at_mov.remove(Dir::Up);
          }
          (1, 0) => {
            liberties_at_mov.remove(Dir::Right);
          }
          (-1, 0) => {
            liberties_at_mov.remove(Dir::Left);
          }
          _ => {}
        }
      }
      self.recalculate_chain_liberties(&scratch[..it+1]);
      eprintln!("made_joins: {}, liberties_at_mov: {}, killed_stones: {}", made_joins, liberties_at_mov, killed_stones);
      if !made_joins && liberties_at_mov.count() == 1 && killed_stones == 1 {
        self.ko_point = Some(Move::new(mov.color.other(), liberties_at_mov.points(mov.pos).iter().find(|opt| opt.is_some()).unwrap().unwrap()));
      }
    }

    self.recalculate_suicide_points(mov.color, &mut scratch);
    self.recalculate_suicide_points(mov.color.other(), &mut scratch);

    Ok(())
  }

  pub fn move_is_legal(&mut self, mov: Move) -> bool {
    mov.pos.fits_in(self.width as i8, self.height as i8) &&
    !self.stone_parents.contains_key(&mov.pos) &&
    !self.suicide_points[mov.color as usize].contains(&mov.pos) &&
    !(self.ko_point.is_some() && self.ko_point.unwrap() == mov)
  }

  pub fn legal_moves(&mut self, color: Color) -> Vec<Coord> {
    let mut result = Vec::with_capacity(self.width as usize * self.height as usize);
    for row in 0..self.height as usize {
      for col in 0..self.width as usize {
        let pos = Coord(col as i8, row as i8);
        if self.move_is_legal(Move::new(color, pos)) {
          result.push(pos);
        }
      }
    }
    result
  }

  pub fn stones(&mut self) -> Vec<Move> {
    self.stone_parents
        .keys()
        .map(|&c| Move::new(Color::Empty, c))
        .collect::<Vec<_>>()
        .iter()
        .map(|m| Move::new(self.color_at(m.pos), m.pos))
        .collect()
  }

  pub fn ataris_for(&self, color: Color) -> usize {
    let all_cores = self.chain_cores.keys().cloned().collect::<Vec<Coord>>();
    all_cores.iter().filter(|&c| self.chain_cores[c] == color && self.chain_liberties[c] == 1).count()
  }

  pub fn stones_in_atari_for(&mut self, color: Color) -> usize {
    self.chain_cores
        .iter()
        .filter(|&(k, &v)| v == color && self.chain_liberties[k] == 1)
        .map(|(&k, &v)| k)
        .collect::<Vec<Coord>>()
        .iter()
        .fold(0, |acc, &c| acc + self.chain_length(c))
  }

  pub fn stones_one_move_to_atari_for(&mut self, color: Color) -> usize {
    self.chain_cores
        .iter()
        .filter(|&(k, &v)| v == color && self.chain_liberties[k] == 2)
        .map(|(&k, &v)| k)
        .collect::<Vec<Coord>>()
        .iter()
        .fold(0, |acc, &c| acc + self.chain_length(c))
  }

  pub fn atari_points_for(&mut self, color: Color, out: &mut [Coord]) -> usize {
    let cores = self.chain_cores
                    .keys()
                    .cloned()
                    .filter(|c| self.chain_cores[c] == color && self.chain_liberties[c] == 1)
                    .collect::<Vec<Coord>>();
    let mut total = 0;
    for c in cores {
      total += self.liberties_of(c, &mut out[total..]);
    }
    total
  }

  pub fn atari_points(&mut self, out: &mut Vec<Coord>) -> usize {
    let cores = self.chain_cores
                    .keys()
                    .cloned()
                    .filter(|c| self.chain_liberties[c] == 1)
                    .collect::<Vec<Coord>>();
    let mut single_liberty = [Coord(-1, -1); 361];
    for &c in &cores {
      self.liberties_of(c, &mut single_liberty);
      out.push(single_liberty[0]);
    }
    cores.len()
  }

  pub fn one_to_atari_points(&mut self, out: &mut Vec<Coord>) -> usize {
    let cores = self.chain_cores
                    .keys()
                    .cloned()
                    .filter(|c| self.chain_liberties[c] == 2)
                    .collect::<Vec<Coord>>();
    let mut two_liberties = [Coord(-1, -1); 361];
    for &c in &cores {
      self.liberties_of(c, &mut two_liberties);
      out.push(two_liberties[0]);
      out.push(two_liberties[1]);
    }
    cores.len()
  }

  pub fn stones_in_suicide_for(&mut self, color: Color) -> usize {
    let suicide_points_copy = self.suicide_points[color as usize].clone();
    let mut cores = Vec::new();
    for &pt in &suicide_points_copy {
      for &neighbour in pt.dilate().iter() {
        if neighbour == pt {
          continue;
        }

        if !self.stone_parents.contains_key(&neighbour) {
          continue;
        }

        let core = self.core_of(neighbour);
        if self.chain_cores[&core] != color {
          continue;
        }

        cores.push(core);
      }
    }

    cores.sort();
    cores.dedup();
    cores.iter().fold(0, |acc, &c| acc + self.chain_length(c))
  }

  pub fn cutting_points_for(&mut self, color: Color) -> Vec<Coord> {
    let mut result = Vec::new();
    let mut scratch = Vec::with_capacity(4);

    let (goban_width, goban_height) = (self.width as usize, self.height as usize);
    for pt in (0..(goban_width * goban_height))
        .map(|it| Coord::from_lin(it, goban_width, goban_height)) {
      // A filled point cannot be a cutting point
      if self.stone_parents.contains_key(&pt) {
        continue;
      }
      scratch.clear();
      scratch.extend(pt.dilate().iter()
                                .cloned()
                                .filter(|&c| c != pt && self.color_at(c) == color));
      scratch.sort();
      scratch.dedup();
      if scratch.len() > 1 {
        result.push(pt);
      }
    }

    result
  }

  pub fn is_simple_eye_for(&mut self, color: Color, point: Coord) -> bool {
    if self.stone_parents.contains_key(&point) {
      return false;
    }

    let libs = Liberties::all_at(point, self.width, self.height);
    let mut cores = Vec::with_capacity(4);
    for l in libs.points(point).iter().cloned().filter(Option::is_some).map(Option::unwrap) {
      if !self.stone_parents.contains_key(&l) {
        return false;
      }
      let core = self.core_of(l);
      if !cores.is_empty() && !cores.contains(&core) {
        return false;
      }
      cores.push(core);
      if self.chain_cores[&core] != color {
        return false;
      }
    }

    true
  }

  pub fn with_move(&self, mov: Move) -> Result<Goban, MoveErr> {
    let mut clone = self.clone();
    clone.push_move(mov)?;
    Ok(clone)
  }

  pub fn intersections(&self) -> usize {
    self.width as usize * self.height as usize
  }

  pub fn to_8layer<N: Copy>(&mut self, for_player: Color, zero: N, one: N) -> Vec<N> {
    let mut result = vec![zero; 19*19*8];

    for it in 0..19 {
      for jt in 0..19 {
        match self.color_at(Coord(it as i8, jt as i8)) {
          Color::Empty => {
            match self.ko_point {
              Some(Move { pos: Coord(col, row), .. }) => result[row as usize*19*8 + col as usize*8 + 6] = one,
              None => (),
            }
          },

          color => {
            let mut layer = if color == for_player {
              0
            } else {
              3
            };
            let mut buf = [Coord(0, 0); 361];
            let core = self.core_of(Coord(it as i8, jt as i8));
            layer += match self.liberties_of(core, &mut buf) {
              1 => 0,
              2 => 1,
              x if x > 2 => 2,
              x => panic!("unexpected number of liberties: {}", x),
            };
            result[it*19*8 + jt*8 + layer] = one;
          },
        }

        result[it*19*8 + jt*8 + 7] = one;
      }
    }

    result
  }

  pub fn get_forbidden<N: Copy>(&mut self, for_player: Color, zero: N, one: N) -> Vec<N> {
    vec![zero; 19*19]
  }
}
