use std::hash::{Hasher, BuildHasher};
use std::mem;

#[derive(Debug, Clone)]
pub struct NoopHasher {
  stream: [u8; 8],
}

impl NoopHasher {
  pub fn new() -> NoopHasher {
    NoopHasher { stream: [0; 8] }
  }
}

impl Hasher for NoopHasher {
  fn finish(&self) -> u64 {
    let arr: [u8; 8] = [self.stream[0],
                        self.stream[1],
                        self.stream[2],
                        self.stream[3],
                        self.stream[4],
                        self.stream[5],
                        self.stream[6],
                        self.stream[7]];
    unsafe { mem::transmute(arr) }
  }
  fn write(&mut self, bytes: &[u8]) {
    let mut written = 0usize;
    for &b in bytes {
      self.stream[written] = b;
      written += 1;
    }
  }
}

#[derive(Clone, Copy)]
pub struct BuildNoopHasher {}

impl BuildNoopHasher {
  pub fn new() -> BuildNoopHasher {
    BuildNoopHasher {}
  }
}

impl BuildHasher for BuildNoopHasher {
  type Hasher = NoopHasher;
  fn build_hasher(&self) -> Self::Hasher {
    NoopHasher::new()
  }
}
